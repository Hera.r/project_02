import requests
from bs4 import BeautifulSoup
from each_category import get_each_book_category


def get_all_book(URL_OF_SITE):
    r = requests.get(URL_OF_SITE)
    r.encoding = 'utf-8'
    if r.ok:
        soup = BeautifulSoup(r.text, "html.parser")

        category_link = soup.find(
            'div', class_="side_categories").find_all('a')

        for link in category_link:
            get_each_book_category("http://books.toscrape.com/"+link['href'])


if __name__ == "__main__":
    get_all_book("http://books.toscrape.com/index.html")
