import requests
from bs4 import BeautifulSoup
import os
import shutil


def details_book(URL):

    r = requests.get(URL)
    r.encoding = 'utf-8'
    if r.ok:
        soup = BeautifulSoup(r.text, "html.parser")
        category = soup.find("ul", "breadcrumb").text.strip().split()[2]
        print(category)

        product_description = soup.find("article")
        list_informations_book = soup.findAll('td')

        all_product_details = product_description.text.strip().splitlines()[
            ::25][0:2]

        link_img = soup.find("img")['src']
        reviews_rating = soup.findAll(
            'p', class_='star-rating')[0].get('class')[1]

        for infos in list_informations_book:
            all_product_details.append(infos.text)

        all_product_details = all_product_details + \
            [URL, category, link_img, reviews_rating]

        head_colonne = ["product_page_url",
                        "title",
                        "product_description",
                        "universal_ product_code (upc)",
                        "category",
                        "price_including_tax",
                        "price_excluding_tax", "number_available",
                        "review_rating", "image_url"]

        head = ' , '.join(head_colonne)
        description = ' , '.join(all_product_details)

        with open(category+".csv", 'a', newline='') as books:
            books.write(head+"\n")
            books.write(description+'\n'+'\n')

        if not os.path.exists("images_book"):
            os.makedirs("images_book")

        link = "http://books.toscrape.com"+link_img[5:]
        name_link = link.split("/")[-1]

        destination_book = os.path.join("images_book", name_link)

        picture_content = requests.get(link, stream=True)
        picture_content.raw.decode_content = True
        with open(destination_book, "wb") as f:
            shutil.copyfileobj(picture_content.raw, f)
