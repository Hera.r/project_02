# Programme d'extraction des prix

### Pré-requis
	Ce qu'il est requis pour commencer avec ce projet:
	Python3, BeautifulSoup

### Installation
	Commencez donc par installer VirtualEnv 
	pip3 install virtualenv 

	créer et activer un environement virtuel
	virtualenv -p python3 env

	Installer les paquets(bibliothèque) répertoriés dans le fichier requirements.txt
	pip3 install -r requirements.txt

### Démarrage
	Déplacez-vous dans votre dossier de travail
	Puis, activer l'environnement, écrivez la commande suivante
	source env/bin/activate

	Lancer le programme
	il suffit de lancer le fichier main.py seulement
	python3 main.py

### Auteur 
	Randrianirina Hera Tenny





