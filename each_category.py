import requests
from bs4 import BeautifulSoup
from books_details import details_book


def get_each_book_category(URL_CATEOGRY):
    r = requests.get(URL_CATEOGRY)
    r.encoding = 'utf-8'
    if r.ok:
        soup = BeautifulSoup(r.text, "html.parser")

        catalogue = soup.find('section')

        li = catalogue.find_all('a')
        try:
            pagination = int(
                soup.find('li', class_='current').text.strip()[-1])
        except AttributeError:
            pagination = 1

        list_link = []
        print(pagination)
        for i in range(1, pagination+1):

            if pagination < 2:
                for element in set(li[:-1]):
                    details_book(
                        "http://books.toscrape.com/catalogue/"+element['href'][9:])

            else:
                URL_new = URL_CATEOGRY[0:-10]+"page-{}.html".format(i)
                r = requests.get(URL_new)
                r.encoding = 'utf-8'
                soup = BeautifulSoup(r.text, "html.parser")
                catalog = soup.find('section')
                li = catalog.find_all('a')

                if len(li) > 21:
                    li = li[:-2]
                else:
                    li = li[:-1]
                for i, element in enumerate(li):
                    if element['href'][9:] not in list_link:
                        list_link.append(element['href'][9:])
                        details_book(
                            "http://books.toscrape.com/catalogue/"+element['href'][9:])
